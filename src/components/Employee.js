import React, { useState, useEffect } from 'react'
import Axios from 'axios'

function Employee () {
  const [data, setData] = useState([])
  const [name, setName] = useState('')
  const [age, setAge] = useState([])
  const [isUpdate, setIsUpdate] = useState(false)

  const handleSubmit = (e, id) => {
    // this is put method
    if (isUpdate) {
      const updateId = localStorage.getItem('id')
      console.log('updatedId :>> ', updateId)
      Axios.put(`https://6364aace7b209ece0f4adfbb.mockapi.io/employee/${updateId}`, {
        name, age
      }).then((res) => {
        console.log('res.data', res.data)
        alert('Edit Successfully.')
      }).then(() => {
        dataShow()
      })
        .catch((error) => {
          console.log('error :>> ', error)
        })
    } else if (!isUpdate) {
      // this is post method
      Axios.post('https://6364aace7b209ece0f4adfbb.mockapi.io/employee', {
        name,
        age
      })
        .then((res) => {
          // setData(res.data)
          alert('Data Added Successfully.')
        }).then(() => {
          dataShow()
        })
        .catch((error) => {
          console.log('error :>> ', error)
        })
    }
    e.preventDefault()
  }
  console.log('data :>> ', data)
  const dataShow = () => {
    Axios.get('https://6364aace7b209ece0f4adfbb.mockapi.io/employee')
      .then((res) => {
        console.log('Get Data from Api :>> ', res.data)
        setData(res.data)
      })
      .catch((error) => {
        console.log('error :>> ', error)
      })
  }

  useEffect(() => {
    dataShow()
  }, [])

  // updateButton For update the state
  const updateButton = (e, id) => {
    setIsUpdate(true)
    localStorage.setItem('id', id)

    data.filter((element, index) => {
      if (id === element.id) {
        console.log('element.id', element.id, id)
        console.log('element.name :>> ', element.name)
        setName(element.name)
        setAge(element.age)
      }
      return element
    }
    )
  }

  // delete Function
  const handleDelete = (e, id) => {
    Axios.delete(`https://6364aace7b209ece0f4adfbb.mockapi.io/employee/${id}`)
      .then((res) => {
        console.log('Deleted Data :>> ', res.data)
        alert('Data Deleted Successfully...')
      }).then(() => {
        dataShow()
      })
      .catch((error) => {
        console.log('error :>> ', error)
      })
  }

  // map method is use to all fetch data in api
  const display = data?.map((data, index) => {
    return (
      <tr key={index}>
          <td>{data.id}</td>
          <td className='tableName'>{data.name}</td>
          <td>{data.age}</td>
          <td className='btn'><button onClick={(e) => handleDelete(e, data.id)}>Delete</button></td>
          <td className='btn'><button onClick={(e) => updateButton(e, data.id)}>Update</button></td>
      </tr>
    )
  })
  return (
    <>
     <div className='container'>
     <div className="title">
        <h1>Registration Form</h1>
      </div>
          <div className='name'>
            <label>Name</label>
            <input type='text' value={name} placeholder='Enter the Name' onChange={(e) => setName(e.target.value)} />
          </div>
          <div className='Age'>
             <label>Age</label>
            <input type='text' value={age} placeholder='Enter the Age' onChange={(e) => setAge(e.target.value)} />
          </div>
        <div className='btn'>
        <button onClick={(e) => handleSubmit(e, data.id)}>Add</button>
        </div>
        <table className='table '>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Delete</th>
                    <th>Update</th>
                </tr>
            </thead>
            <tbody>
          {display}
            </tbody>
        </table>
    </div>
    </>
  )
}

export default Employee
